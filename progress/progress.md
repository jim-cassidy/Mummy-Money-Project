


<h1 style="color:blue;">Progress</h1>




<h2 style="color:red;">Introduction</h2>

My 1st day of working on project:

<p style="color:green;">
The first step was understanding what needs to be done.
My assignment was to create a pyramid simulation, using attributes
so that different members can rise up at different rates based on their
abilities.  My assumption is that some attributes are more useful than others
to make it to the top.

The first task for me was to look over best practices before I start.
I am learning now how to program better in python looking over other programers
recommendations.  I also looked over best practies for GIT, and also building
a 12-factor app.

I began working on the setup python file, which creates a database
and populates the data with attributes.  I started off with a normal probability
distribution, of values between 0 and 1, .5 being the mean.

</p>

-------

<h2 style="color:red;">Second day</h2>

I know have the setup, it will generate a population of 100 investors.

* found bug - can fix value over 1 or below 0, but doesn't check replacement value correctly
*  added fields 'have parent' and 'active' to database
*  make a new table to keep track of pyramid tree
* started working on main mummy program
* able to fetch 10 random children from population
* in setup, added to table pyramid the children of the mummy
*  added to the main table the fact that some investors have a parent ( the mummy )

<h2 style="color:red;">Third day</h2>

* started main mummy.py file
* calculated chance of active members finding another
* started a powerpoint collection of screenshots and code
* tested the investor data was going into SqLite database correctly
* added a documentation file, from python best practices suggestions
* have Flash and Pusher working, and I see that the backend is up and running
* each turn, now checks to see if a member found someone
* checks to see if member will accept
* when a member accepts, it changes a member to active
* updates pyramid table to keep track of each parent / child
* fixed bug in mummy program - didn't write permanently to pyramid file after finished
* improved code, made it much cleaner, easier to read, applying concepts of python best practices

<h2 style="color:red;">Fourth Day</h2>

* when a member recruits another, they get $
* made a loop to run main program for each week, all data in databse seems to be find ( no duplicates, money correct )
* got Vue up and running, able to write a message to the screen
* using the setup.py, will delete the database file if it exists, so it starts fresh
* made a new testing screenshots file in a document form, looks a little better
* Achieved asynchronous communication with python and Vue!





</p>



© 2019 GitHub, Inc.
Terms
Privacy
Security
Status
Help
Contact GitHub
Pricing
API
Training
Blog
About
