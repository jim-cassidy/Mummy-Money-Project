# MIT License
#
# Copyright (c) [year] [fullname]
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRAN#TY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# - IMPORTS -

import sqlite3
import numpy as np


# ------------ 
# create database

sqlite_file = 'investors.sqlite'    # name of the sqlite database file
table_name1 = 'investors'    
investorID = 'ID' # name of the column
numinvestors = 100
money = -500


# ** populate database with stats - using a normal distribution **

innocence = np.random.normal(.5,.12,100) 

for x in range(numinvestors):
    if innocence[x] > 1:
        print("greater or less than 1")
        while s[x] > 1:
            innocence[x] = np.random.normal(.5,.12,1)
        while s[x] < 1:
            innocence[x] = np.random.normal(.5,.12,1)


experience = np.random.normal(.5,.12,100) 

for x in range(numinvestors):
    if experience[x] > 1:
        print("greater or less than 1")
        while s[x] > 1:
            experience[x] = np.random.normal(.5,.12,1)
        while s[x] < 1:
            experience[x] = np.random.normal(.5,.12,1)


charisma = np.random.normal(.5,.12,100) 

for x in range(numinvestors):
    if  charisma[x] > 1:
        print("greater or less than 1")
        while s[x] > 1:
            charisma[x] = np.random.normal(.5,.12,1)
        while s[x] < 1:
            charisma[x] = np.random.normal(.5,.12,1)


# Connecting to the database file(`id`, `name`, `birth_date`, `age`) 
conn = sqlite3.connect(sqlite_file)
c = conn.cursor()

# Create the table
c.execute ("CREATE TABLE IF NOT EXISTS customers (ID integer(11), innocence float(1), experience float(1), charisma float(1), money int(1))")


# fill in the table with investors stats


for x in range(numinvestors):
    c.execute("INSERT INTO customers VALUES(?, ?, ?, ?, ?)", (x, innocence[x], experience[x], charisma[x], -500))


for row in c.execute('SELECT * FROM customers'):
        print(row)

# Committing changes and close
conn.commit()
conn.close()

# -------------

      
print "done!"

# - MAIN -















