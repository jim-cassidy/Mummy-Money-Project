# MIT License
#
# Copyright (c) [year] [fullname]
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRAN#TY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# - IMPORTS -

import sqlite3
import numpy as np
import random

# ------------ 
# create database

sqlite_file = 'investors.sqlite'    # name of the sqlite database file
table_name1 = 'investors'  # the table name is investors
investorID = 'ID' # name of the column
numinvestors = 100 # number of investors
money = -500 # -$500 every investor starts out


# ** populate database with stats - using a normal distribution **

innocence = np.random.normal(.5,.12,100)   # Each member will hve an innocence value.

for x in range(numinvestors):
    if innocence[x] > 1:
        # For the normal distribtion, we still need to cut off if over 1 or below 0
        while innocence[x] > 1:
            innocence[x] = np.random.normal(.5,.12,1)
        while innocence[x] < 1:
            innocence[x] = np.random.normal(.5,.12,1)


experience = np.random.normal(.5,.12,100) # Each member will have an experiene value.

for x in range(numinvestors):
    if experience[x] > 1:
        # For the normal distribtion, we still need to cut off if over 1 or below 0
        while experience[x] > 1:
            experience[x] = np.random.normal(.5,.12,1)
        while experience[x] < 1:
            experience[x] = np.random.normal(.5,.12,1)


charisma = np.random.normal(.5,.12,100)  # Each member will have a charisma value

for x in range(numinvestors):
    if  charisma[x] > 1:
        # For the normal distribtion, we still need to cut off if over 1 or below 0
        while charisma[x] > 1:
            charisma[x] = np.rwhereandom.normal(.5,.12,1)
        while charisma[x] < 1:
            charisma[x] = np.random.normal(.5,.12,1)

print ( "Added 100 values for innocence, experience, and charisma" )

# Connecting to the database file(`id`, `name`, `birth_date`, `age`) 
conn = sqlite3.connect(sqlite_file)
c = conn.cursor()

# Create the table
c.execute ("CREATE TABLE IF NOT EXISTS investors (ID integer(11) PRIMARY KEY, innocence float(1), experience float(1), charisma float(1), money int(1), status varchar(10), hasparent int(11) ) ")

print ("Investor table created if it doesn't exist already...")

# Here we will in the table with investors stats

for x in range(numinvestors):
    c.execute("INSERT INTO investors VALUES(?, ?, ?, ?, ?, ?, ?)", (x, innocence[x], experience[x], charisma[x], -500, 'INACTIVE', 0))

print ("Investor's values inserted into table...")

## - Create table if not exits for pyramid
c.execute ("CREATE TABLE IF NOT EXISTS pyramid ( PARENT_ID integer(11), CHILDID integer(11))")

print ("Pyramid table created if did not exist already...")


### Mummy will select 10 children

select10 = random.sample(range(100), 10)  # list to  hold 10 children
for x in range (10):
    select10[x] = select10[x] + 1 # -- mummy can't select itself, so I added one


## update parent table so that mummy has parents

for x in range(10):
    c.execute("INSERT INTO pyramid VALUES(?, ?)", ( 0, select10[x] ))
    
print ("Mummy has 10 children now, inserted into pyramid table...")
        
for x in range ( 10 ):
    userid = select10[x]
    newphone = 1
    stat = 'ACTIVE'

    c.execute('''UPDATE investors SET hasparent = ? WHERE id = ? ''', (newphone, userid))
    c.execute('''UPDATE investors SET status = ? WHERE id = ? ''', (stat, userid))



# Committing changes and close
conn.commit()
conn.close()
      
print ("configuration done!")
















