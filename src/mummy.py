#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRAN#TY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# - IMPORTS -

import sqlite3
import numpy as np
import random
import math

# ------------ 
# create database

sqlite_file = 'investors.sqlite'   
table_name1 = 'investors'  
investorID = 'ID' 
numinvestors = 100
money = -500 # -$500 every investor starts out

##  --- findnewmember will add members sometime to existing active members.

def findnewmember():    
  chancefind = 0
  directmembers = 3 # for now as if has 3 members, need to do a database search for number of actual members
  newinvestor = 0
  print("Hello from a function")
  aaa = 0
  hasmoney = 0

  for row in c.execute("SELECT * FROM investors WHERE status = 'ACTIVE'"):  # search database for active members
         print("trying to find a new member... for member:  " , row[0])
         print ( 1 - math.log(3, 10))  # see if member will
         chancefind = row[2] * row[3] * ( 1 - math.log(directmembers,10))
         randomnum = random.random()
         print ("randomnum: " ,randomnum)
         print ("chancefind: ", chancefind)
    
         if ( randomnum > chancefind ):
             print ("foundone!!!")
             doesaccept = row[1] * ( 1 - row[3] )
             randomnum = random.random()
             if ( randomnum > doesaccept ):
                 print ("new investor!!!!!!")
               # Let us find another member that is inactive
                 for row2 in c2.execute("SELECT ID FROM investors WHERE status = 'INACTIVE'"):
                     #print ( row2[0] )
                     newinvestor = row2[0]
                     print ( "parent is " , row[0] )
                     print ( "new investor is " , newinvestor) 
	             # add to investor table that a member is active
                     c3.execute('''UPDATE investors SET status = ? WHERE id = ? ''', ('ACTIVE', newinvestor))
                     # add to pyramid table to keep track of children
                     c4.execute('''INSERT INTO pyramid VALUES ( ? , ? ) ''', ( row[0] , newinvestor))
                     #print ( row2[0] , " -- " , newinvestor )
                     # add $100 for sucessful recruit
                     hasmoney = row[4]
                     print ("has money -->" , hasmoney  )
                     hasmoney = row[4] + 100
	             c5.execute('''UPDATE investors SET money = ? WHERE id = ? ''', (hasmoney, row[0]))
                     break
         else:
             print ( "Did not find a member for: " , row[0])
 

# ------------------

# Connect to databases.  We need to open more than one connection.        


conn = sqlite3.connect(sqlite_file)
c = conn.cursor()

conn2 = sqlite3.connect(sqlite_file)
c2 = conn.cursor()

conn3 = sqlite3.connect(sqlite_file)
c3 = conn.cursor()

conn4 = sqlite3.connect(sqlite_file)
c4 = conn.cursor()

conn5 = sqlite3.connect(sqlite_file)
c5 = conn.cursor()
conn5 = sqlite3.connect(sqlite_file)
c5 = conn.cursor()

running = 1

directmembers = 3


findnewmember()

for row in c.execute("SELECT * FROM investors"):  # search database for active members
    print ( row )

for row in c.execute("SELECT * FROM pyramid"):  # search database for active members
    print ( row )


## Here we will close the connection so it permanently writes to database

conn.commit()
conn.close()
  


    










